Autor: Jakub Biskup
Rok akademicki: 2017/2018
Semestr: 5
Grupa: 1
Sekcja: 1

Krótki opis projektu:
Program generujący negatyw bitmapy. Wartość kanału koloru jest odejmowana od maksymalnej wartości kanału, tj. 255.
Zmieniany jest każdy z kanałów koloru oprócz kanału przezroczystości (A).
Użytkownik ma do dyspozycji prosty interfejs, w którym może wybrać plik z okna dialogowego, wybrać DLL, liczbę wątków.
Po wygenerowaniu bitmapy zostaje wyświetlone okienko z czasem trwania przetwarzania. Po całej operacji plik można zapisać na dysk.