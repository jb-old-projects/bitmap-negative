﻿/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Negatyw bitmapy
/* Przedmiot: Języki Asemblerowe
/* Prowadzący: mgr. inż. Jarosław Paduch
/* Wersja aplikacji: 1.0 
/* Wersja pliku: 0.12
/* Data ostatniej modyfikacji: 27 stycznia 2018
/*
/* Historia zmian 0.11 -> 0.12:
/* - Dodanie nagłówka zawierającego historię zmian oraz aktualną wersję
/* - Dodanie sprawdzenia czy komputer obsługuje AVX
/* 
/* Historia zmian 0.10 -> 0.11:
/* - Usuwanie niepotrzebnych części kodu, komentarze
/* 
/* Historia zmian 0.09 -> 0.10:
/* - Poprawienie działania wątków
/* - Zmiany w logice programu (głównie związane z zabezpieczeniami)
/* 
/* Historia zmian 0.08 -> 0.09:
/* - Dodane zostało odczytywanie ilości RAMu danego urządzenia i ustalanie od niej wartości maksymalnej zdjęcia
/* - Delikatne przeczyszczenie kodu
/* 
/* Historia zmian 0.07 -> 0.08:
/* - Dodana została możliwość pliku po wygenerowaniu (w formacie JPG oraz BMP)
/* - Zostało dodane zabezpieczenie, które ma zapobiec nadpisywaniu nagłówka pliku BMP
/* 
/* Historia zmian 0.06 -> 0.07:
/* - Dodaną została procedura negująca w DLLce asemblera (z użyciem rozkazów wektorowych)
/* 
/* Historia zmian 0.05 -> 0.06:
/* - Dodaną została procedura negująca w DLLce asemblera
/* 
/* Historia zmian 0.04 -> 0.05:
/* - Została dodana zaślepkowa logika przy wyborze DLLki asemblera
/* - Dodana możliwość zaślepkową zapisu
/* 
/* Historia zmian v0.03 -> v0.04:
/* - Przeniesienie pliku do obecnego folderu
/* 
/* Historia zmian v0.02 -> v0.03:
/* - Zostało dodane sprawdzenie czasu wykonania DLLki
/* - Po zakończeniu przetwarzania bitmapy wyświetlany zostaje message box z czasem trwania przetwarzania
/* - Zostało dodane ustawienie optymalnej liczby wątków dla danego komputera
/* 
/* Historia zmian v0.01 -> v0.02:
/* - Została dodana obsługa wątków (od 1 do 64)
/* - Zostały dodane metody pomocnicze
/* 
/* Historia zmian v0.00 -> v0.01:
/* - Został stworzony podstawowy interfejs użytkownika, w którym można wybrać plik za pomocą okna dialogowego
/* - Został dodany podgląd obecnie wybranej bitmapy
/* - Została dodana przykładowa metoda generująca negatyw bitmapy (jeszcze nie jako DLL)
/**********************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace BMP_Negative
{
    public partial class MainWindow : Form
    {
        #region DLLImports

        [DllImport("C:\\Users\\Jakub\\Source\\Repos\\JA_J.Biskuo-Negatyw-Bitmapy\\Zrodla\\x64\\Release\\CppDLL.dll")]
        public static extern void GenerateNegativeCpp(byte[] byteArr, int firstIndex, int lastIndex);

        [DllImport("C:\\Users\\Jakub\\Source\\Repos\\JA_J.Biskuo-Negatyw-Bitmapy\\Zrodla\\x64\\Release\\AssemblerDLL.dll")]
        public static extern int GenerateNegativeAsm(byte[] byteArr, int lowerLimit, int upperLimit);

        [DllImport("C:\\Users\\Jakub\\Source\\Repos\\JA_J.Biskuo-Negatyw-Bitmapy\\Zrodla\\x64\\Release\\AssemblerDLL.dll")]
        public static extern int GenerateNegativeAsmAVX(byte[] byteArr, int lowerLimit, int upperLimit);

        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetPhysicallyInstalledSystemMemory(out long TotalMemoryInKilobytes);

        [DllImport("kernel32.dll")]
        private static extern long GetEnabledXStateFeatures();

        #endregion

        #region Variables

        private OpenFileDialog ofd = new OpenFileDialog();
        private SaveFileDialog sfd = new SaveFileDialog();
        private Bitmap sourceBmp = null;
        private Bitmap resultBmp = null;
        private ChosenDLL chosenDLL = ChosenDLL.None;

        private float maxSafeImageSizeInGB;
        private int computerRamInGB;
        private bool avxAvailable;

        private const int OFFSET_TO_BMP_PIXEL_DATA_INFO = 10;

        public enum ChosenDLL
        {
            None,
            Cpp,
            Asm,
            AsmAVX
        }

        #endregion

        #region Methods

        /// <summary>
        /// Main window class constructor.
        /// </summary>

        public MainWindow()
        {
            InitializeComponent();
            SetOptimalThreadsNumber();
            SetComputerRamAmount();
            SetMaxSafeImageSize();
            CheckAVX();
            button4.Enabled = false;
            //OnOpenFileButtonClick(null, null);
        }

        /// <summary>
        /// SetOptimalThreadsNumber is a method which is reading available processor count and setting trackbar value to that count.
        /// </summary>

        private void SetOptimalThreadsNumber()
        {
            trackBar1.Value = Environment.ProcessorCount;
            label4.Text = trackBar1.Value.ToString();
        }

        /// <summary>
        /// CheckAVX is a method which is reading if processor and OS supports AVX.
        /// </summary>

        private void CheckAVX()
        {
            try
            {
                avxAvailable = (GetEnabledXStateFeatures() & 4) != 0;
            }
            catch
            {
                avxAvailable = false;
            }
        }

        /// <summary>
        /// SetComputerRamAmount is a method which is reading available computer RAM memory count.
        /// </summary>

        private void SetComputerRamAmount()
        {
            long ramInKB;
            GetPhysicallyInstalledSystemMemory(out ramInKB);

            computerRamInGB = (int)(ramInKB / 1024 / 1024);
        }

        /// <summary>
        /// SetMaxSafeImageSize is a method which is setting maximum safe image size in GB (it's necessary due to memory allocation).
        /// </summary>

        private void SetMaxSafeImageSize()
        {
            maxSafeImageSizeInGB = computerRamInGB * 0.125f;
        }

        /// <summary>
        /// TryUnlockGenerateButton is a method which is called after every change in application that could unlock generate button.
        /// </summary>

        private void TryUnlockGenerateButton()
        {
            if (textBox1.Text != string.Empty && (radioButton1.Checked || radioButton2.Checked || radioButton3.Checked))
            {
                button2.Enabled = true;
            }
        }

        /// <summary>
        /// GenerateNegativeByteArray is a method which is called after click on generate button.
        /// </summary>
        /// <param name="byteArr">Two dimensional byte array.</param>
        /// <returns></returns>

        private byte[] GenerateNegativeByteArray(byte[] byteArr)
        {
            int bmpPixelDataStartIndex = byteArr[OFFSET_TO_BMP_PIXEL_DATA_INFO];

            int threadAmount = Convert.ToInt32(label4.Text);
            int threadIndexRange = (byteArr.Length - bmpPixelDataStartIndex) / threadAmount;

            List<Thread> threads = new List<Thread>();

            for (int i = 0; i < threadAmount; i++)
            {
                Thread thread = null;
                int tmpNextStartIndex = 0;
                int tmpNextLastIndex = 0;

                tmpNextStartIndex = bmpPixelDataStartIndex + threadIndexRange * i;
                tmpNextLastIndex = tmpNextStartIndex + threadIndexRange;

                if (i != threadAmount - 1)
                {
                    tmpNextLastIndex = tmpNextStartIndex + threadIndexRange;
                }
                else
                {
                    tmpNextLastIndex = byteArr.Length;
                }

                if (chosenDLL == ChosenDLL.Cpp)
                {
                    thread = new Thread(() => GenerateNegativeCpp(byteArr, tmpNextStartIndex, tmpNextLastIndex));
                }
                else if (chosenDLL == ChosenDLL.Asm)
                {
                    thread = new Thread(() => GenerateNegativeAsm(byteArr, tmpNextStartIndex, tmpNextLastIndex));
                }
                else if (chosenDLL == ChosenDLL.AsmAVX)
                {
                    thread = new Thread(() => GenerateNegativeAsmAVX(byteArr, tmpNextStartIndex, tmpNextLastIndex));
                }

                threads.Add(thread);
            }

            Stopwatch timer = Stopwatch.StartNew();

            foreach (var thread in threads)
            {
                thread.Start();
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            timer.Stop();

            threads.Clear();

            MessageBox.Show("GenerateNegative method has finished its operation!\n\t\tTotal duration: " + timer.ElapsedTicks.ToString() + "ms\n\tNow the result image will be rendered!", "Info");

            return byteArr;
        }

        #region Callbacks

        private void OnGenerateNegativeButtonClick(object sender, EventArgs e)
        {
            button2.Enabled = false;

            resultBmp = ByteArrayToBitmap(GenerateNegativeByteArray(ImageToByteArray2D(sourceBmp)));
            pictureBox2.Image = resultBmp;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            button4.Enabled = true;
        }

        private void OnOpenFileButtonClick(object sender, EventArgs e)
        {
            if (sourceBmp != null)
            {
                sourceBmp.Dispose();
                pictureBox1.Image = null;
            }
            if (resultBmp != null)
            {
                resultBmp.Dispose();
                pictureBox2.Image = null;
            }

            ofd.Filter = "bmp|*.bmp";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //ofd.FileName = "C:\\Users\\Jakub\\Source\\Repos\\JA_J.Biskuo-Negatyw-Bitmapy\\Plik_exe\\Dane testowe\\bmp183MB.bmp";
                textBox1.Text = ofd.FileName;
                textBox2.Text = ofd.SafeFileName;

                //trackBar1.Value = 4;
                //label4.Text = trackBar1.Value.ToString();
                //radioButton3.Checked = true;

                using (FileStream ms = new FileStream(ofd.FileName, FileMode.Open, FileAccess.Read))
                {
                    long bmpSizeInB = ms.Length;

                    double bmpSizeInGB = bmpSizeInB / 1024 / 1024 / 1024;

                    if (bmpSizeInGB < maxSafeImageSizeInGB)
                    {
                        sourceBmp = new Bitmap(ofd.FileName);
                        pictureBox1.Image = Image.FromFile(ofd.FileName);
                        pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                        TryUnlockGenerateButton();
                    }
                    else
                    {
                        MessageBox.Show("The image is too large to be displayed safely! Choose another one.", "Error");
                    }
                }
            }
        }

        private void OnSaveFileButtonClick(object sender, EventArgs e)
        {
            sfd.Filter = "jpg|*.jpg|bmp|*.bmp";
            sfd.Title = "Save an Image File";

            sfd.ShowDialog();

            if (sfd.FileName != "" && resultBmp != null)
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(sfd.FileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        using (Bitmap tmpBmp = new Bitmap(resultBmp))
                        {

                            switch (sfd.FilterIndex)
                            {
                                case 1:
                                    tmpBmp.Save(memory, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    break;
                                case 2:
                                    tmpBmp.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                                    break;
                            }

                            byte[] bytes = memory.ToArray();
                            fs.Write(bytes, 0, bytes.Length);

                            tmpBmp.Dispose();
                        }
                        fs.Dispose();
                    }
                    memory.Dispose();
                }
            }
        }

        private void OnAboutButtonClicked(object sender, EventArgs e)
        {
            AboutPopupWindow tmpWindow = new AboutPopupWindow();
            tmpWindow.Show();
        }

        private void OnCppDllButtonClicked(object sender, EventArgs e)
        {
            chosenDLL = ChosenDLL.Cpp;
            TryUnlockGenerateButton();
        }

        private void OnAsmDllButtonClicked(object sender, EventArgs e)
        {
            if(avxAvailable)
            {
                chosenDLL = ChosenDLL.Asm;
                TryUnlockGenerateButton();
            }
            else
            {
                MessageBox.Show("Processor / OS does not support AVX!", "Warning");
            }
        }

        private void OnAsmAvxDllButtonClicked(object sender, EventArgs e)
        {
            if (avxAvailable)
            {
                chosenDLL = ChosenDLL.AsmAVX;
                TryUnlockGenerateButton();
            }
            else
            {
                MessageBox.Show("Processor / OS does not support AVX!", "Warning");
            }
        }

        private void OnTrackBarScroll(object sender, EventArgs e)
        {
            label4.Text = trackBar1.Value.ToString();
        }

        #endregion

        #region Converting Methods

        /// <summary>
        /// ImageToByteArray2D is a method which is converting Bitmap object to two dimensional byte array.
        /// </summary>
        /// <param name="img">Bitmap object.</param>
        /// <returns></returns>

        private byte[] ImageToByteArray2D(Bitmap img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// ByteArrayToBitmap is a method which is converting two dimensional byte array to Bitmap object.
        /// </summary>
        /// <param name="byteArr"></param>
        /// <returns></returns>

        private Bitmap ByteArrayToBitmap(byte[] byteArr)
        {
            using (var ms = new MemoryStream(byteArr))
            {
                return new Bitmap(ms);
            }
        }

        #endregion

        #endregion
    }
}
