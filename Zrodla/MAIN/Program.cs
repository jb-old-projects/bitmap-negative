﻿/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Negatyw bitmapy
/* Przedmiot: Języki Asemblerowe
/* Prowadzący: mgr. inż. Jarosław Paduch
/* Data ostatniej modyfikacji: 22 stycznia 2018
/* 
/* Opis: Jest to główna klasa aplikacji, w której znajduje się metoda Main.
/**********************************************/

using System;
using System.Windows.Forms;

namespace BMP_Negative
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}
