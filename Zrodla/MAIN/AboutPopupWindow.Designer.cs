﻿/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Negatyw bitmapy
/* Przedmiot: Języki Asemblerowe
/* Prowadzący: mgr. inż. Jarosław Paduch
/* Data ostatniej modyfikacji: 27 stycznia 2018
/* 
/* Opis: Jest to klasa definiująca widok message boxa wyświetlanego po kliknięciu przycisku About na głównym ekranie aplikacji. 
/**********************************************/

using System.Windows.Forms;

namespace BMP_Negative
{
    public partial class AboutPopupWindow : Form
    {
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label1;

        public AboutPopupWindow()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            SuspendLayout();
            // 
            // Title
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Arial Narrow", 20F);
            label1.Location = new System.Drawing.Point(52, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(174, 31);
            label1.TabIndex = 0;
            label1.Text = "Bitmap Negative";
            // 
            // Author
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            label2.Location = new System.Drawing.Point(12, 77);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(157, 23);
            label2.TabIndex = 1;
            label2.Text = "Author: Jakub Biskup";
            // 
            // Version
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            label3.Location = new System.Drawing.Point(12, 142);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(103, 23);
            label3.TabIndex = 2;
            label3.Text = "Version: 1.0";
            // 
            // Subject
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            label4.Location = new System.Drawing.Point(12, 109);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(221, 23);
            label4.TabIndex = 3;
            label4.Text = "Subject: Assembler languages";
            // 
            // AboutPopupWindow
            // 
            ClientSize = new System.Drawing.Size(284, 261);
            Controls.Add(this.label4);
            Controls.Add(this.label3);
            Controls.Add(this.label2);
            Controls.Add(this.label1);
            Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            Name = "AboutPopupWindow";
            Text = "About";
            ResumeLayout(false);
            PerformLayout();
        }
    }
}