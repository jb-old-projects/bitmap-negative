/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Negatyw bitmapy
/* Przedmiot: J�zyki Asemblerowe
/* Prowadz�cy: mgr. in�. Jaros�aw Paduch
/* Wersja pliku: 0.05
/* Data ostatniej modyfikacji: 22 stycznia 2018
/*
/* Opis: Plik zawieraj�cy metod� udost�pnian� przez bibliotek�.
/*
/* Historia zmian 0.04 -> 0.05:
/* - dodanie nag��wka zawieraj�cego histori� zmian oraz aktualn� wersj�
/*
/* Historia zmian 0.03 -> 0.04:
/* - zmiana nazwy metody
/*
/* Historia zmian 0.02 -> 0.03:
/* - przeniesieni pliku do obecnego folderu
/*
/* Historia zmian 0.01 -> 0.02:
/* - dodane zosta�y parametry do funkcji, dodane zosta�o cia�o funkcji
/*
/* Historia zmian 0.00 -> 0.01:
/* - stworzenie pustego pliku
/**********************************************/

extern "C" __declspec(dllexport) void GenerateNegativeCpp(unsigned char byteArray[], int firstIndex, int lastIndex)
{
	for (int i = firstIndex; i < lastIndex; i++)
	{
		byteArray[i] = 255 - byteArray[i];
	}
}