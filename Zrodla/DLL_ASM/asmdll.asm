;;-------------------------------------------------------------------------------------------------
;; Autor: Jakub Biskup
;; Temat: Negatyw bitmapy
;; Przedmiot: J�zyki Asemblerowe
;; Prowadz�cy: mgr. in�. Jaros�aw Paduch
;; Wersja pliku: 0.06
;; Data ostatniej modyfikacji: 22 stycznia 2018
;;
;; Opis: Plik zawieraj�cy metod� udost�pnian� przez bibliotek� w wersji asemblerowej.
;;
;; Historia zmian 0.05 -> 0.06:
;; - dodanie nag��wka zawieraj�cego histori� zmian oraz aktualn� wersj�
;;
;; Historia zmian 0.04 -> 0.05:
;; - dodanie metody stosuj�cej operacje wektorowe wraz z komentarzami
;;
;; Historia zmian 0.03 -> 0.04:
;; - usuni�cie tymczasowego cia�a funkcji oraz dodanie metody niewektorowej z komentarzami
;;
;; Historia zmian 0.02 -> 0.03:
;; - dodanie tymczasowego cia�a funkcji, kt�ry przyjmowa� dwa parametry typu dword i zwraca� ich sum�
;;
;; Historia zmian 0.01 -> 0.02:
;; - przeniesienie pliku do obecnego folderu
;;
;; Historia zmian 0.00 -> 0.01:
;; - stworzenie pustego pliku
;;-------------------------------------------------------------------------------------------------

.DATA
neg_const byte 11111111b		; zmiennna potrzebna do operacji negacji - dokladniej opisana ni�ej
.CODE

;--------------------------------------------------------------------------------------------------
;						Opis procedur GenerateNegativeAsm oraz GenerateNegativeAsmAVX
; 
; 
; --- GenerateNegativeAsm ---
; 
; Procedura wykonuj�ca negacj� tablicy bajt�w zawieraj�cej informacje o kana�ach RGB koloru.
; Procedura przyjmuje 3 parametry. Pierwszym parametrem jest wy�ej opisana tablica bajt�w.
; Drugim oraz trzecim parametrem s� zmienne typu DWORD, kt�re oznaczaj� kolejno
; pocz�tek oraz koniec zakresu tablicy, na kt�rym ma dzia�a� algorytm.
;
;
; --- GenerateNegativeAsmAVX ---
; 
; Procedura ma to samo dzia�anie co procedura GenerateNegativeAsm, jednak�e r�nic� jest to,
; �e zastosowane zosta�y w niej rozkazy wektorowe. Do realizacji zosta�y u�yte rejestry ymm,
; poniewa� maj� one rozmiar a� 256 bit�w (tj. 32 bajt�w). U�yte mog�y by� r�wnie� rejestry xmm,
; jednak�e wi�kszy rozmiar rejestr�w powinien zapewni� szybsze wykonanie samych operacji.
; Procedura co iteracje sprawdza, czy pozosta�a ilo�� danych jest wystarczaj�ca do zape�nienia
; rejestru ymm. Je�eli rejestr ymm jest w stanie by� pomy�lnie zape�niony to nast�puje wywo�anie
; odpowiednich operacji wektorowych. Jednak�e je�eli nie dysponujemy odpowiedni� ilo�ci� danych to
; zostanie wywo�ana wywo�ana standardowa sekwencja operacji, kt�re s� wykonywane w procedurze GenerateNegativeAsm.
; 
;--------------------------------------------------------------------------------------------------

;--------------------------------------------------------------------------------------------------
;											Komentarz
; Rejestr RCX przechowuje adres pocz�tka tablicy (og�lnie - adres pierwszego parametru)
; Rejestr RDX przechowuje adres drugiego parametru (tj. pocz�tek zakresu tablicy do edytowania)
; Rejestr R8 przechowuje adres trzeciego parametru (tj. koniec zakresu tablicy do edytowania)
; Kolejne adresy znajduj� si� w R9, [RBP + 48], [RBP + 56], [RBP + 64], [RBP + 72], [RBP + 80]
; 
; Algorytm negowania obrazu polega na odpowiedniej zmianie warto�ci danego kana�u, to jest od
; warto�ci maksymalnej (255 w systemie dziesi�tnym lub 11111111 w systemie binarnym) odejmowana
; jest warto�� obecna kana�u. Taki sam efekt (w�a�ciwie wskazuje na to sama nazwa)
; zosta�by otrzymany poprzez zanegowanie danej warto�ci.
; 
;											Przyk�ad 
; warto�� kana�u = 64 (01000000 binarnie) 
; warto�� po zastosowaniu algorytmu = 191 (10111111 binarnie)
; Jak wida� 191 jest negacj� liczby 64. 
; 
; W programie zastosowany zosta� rozkaz xor. Operacja xor zwraca warto�� 1 dla r�nych bit�w,
; a 0 dla tych samych. Je�eli wykonamy operacj� xor u�ywaj�c jednej zmiennej sk�adaj�cej si� z
; samych 1 to efektem tego b�dzie zanegowanie drugiej zmiennej.
;--------------------------------------------------------------------------------------------------

GenerateNegativeAsm proc tab: byte, min: dword, max: dword

mainLoop:
	mov al, [rcx+rdx]			; wczytanie kom�rki do 8 bitowego rejestru AL. Na pocz�tku jest to adres
								; kom�rki, od kt�rej ma zacza� by� edytowana tablica (czyli index pocz�tkowy + offset)
	xor al, neg_const			; negacja warto�ci, kt�ra si� znajdowa�a w rejestrze AL
								; zastosowanie rozkazu xor zosta�o wyja�nione w powy�szym komentarzu
	mov [rcx+rdx], al			; zapisanie zanegowanej warto�ci do jej miejsca w tablicy
	inc rdx						; inkrementacja indeksu tablicy
	cmp rdx, r8					; sprawdzenie czy zakres operacji na tablicy nie zosta� przekroczony
	je procEnd					; je�li zosta�o wykryte przekroczenie zakresu, to ko�czymy dzia�anie procedury
								; skokiem do etykiety procEnd
	jmp mainLoop				; w innym przypadku (je�eli nie zosta� przekroczony zakres) to przeskakujemy
								; do pocz�tku p�tli mainLoop i wykonujemy dzia�anie dla kolejnej warto�ci

procEnd:
	ret							; wyj�cie z procedury GenerateNegativeAsm

GenerateNegativeAsm endp

;--------------------------------------------------------------------------------------------------

GenerateNegativeAsmAVX proc tab: byte, min: dword, max: dword
		
	vpcmpeqb ymm1, ymm3, ymm3	; rozkaz vpcmpeqb sprawdza, czy zawarto�� rejestr�w z argumentu drugiego i trzeciego s� sobie r�wne
								; je�eli s� sobie r�wne, to zape�nia rejestr z argumentu pierwszego samymi jedynkami
								; rejestr z argumentu pierwszego (w tym przypadku ymm1) b�dzie pe�ni� rol� odpowiednika
								; sta�ej neg_const dla operacji wektorowych
	jmp checkAddressRange		; skok do etykiety sprawdzaj�cej czy nie wykroczyli�my poza zakres
	
loopWithMMX:
	vmovups ymm2, [rcx+rdx]		; rozkaz vmovups s�u�y do przenoszenia danych z argumentu drugiego do argumentu pierwszego
								; w tym przypadku zostan� przeniesione 32 bajty danych z naszej tablicy do rejestru ymm2
	vxorpd ymm0, ymm1, ymm2		; rozkaz vxorpd s�u�y do wykonania operacji xor drugiego i trzeciego argumentu, a nast�pnie
								; zapis wyniku do argumentu pierwszego
								; w tym przypadku zostanie wykonana operacja xor na rejestrze ymm1 (kt�ry zosta� na pocz�tku
								; wykonywania procedury GenerateNegativeAsmAVX wype�niony jedynkami) oraz rejestrze ymm2
								; wynik operacji zostanie zapisany w rejestrze ymm0
	vmovups [rcx+rdx], ymm0		; przeniesienie zanegowanych danych z rejestru ymm0 na ich miejsce w naszej tablicy
	add rdx, 20h				; dodanie 32 do adresu naszej tablicy
	cmp rdx, r8					; sprawdzenie, czy nie zosta� przekroczony zakres operacji na tablicy
	je procEnd					; je�li zosta�o wykryte przekroczenie zakresu, to ko�czymy dzia�anie procedury
								; skokiem do etykiety procEnd 
	jmp checkAddressRange		; skok do etykiety checkAddressRange, kt�ra na nowo sprawdzi czy nie zostany przekroczony zakres
								; nast�pnej iteracji i zdecyduje w jaki spos�b kontynuowa� program
	
checkAddressRange:				; etykieta checkAddressRange, w kt�rej sprawdzamy czy nie zostanie przekroczony zakres przy danej iteracji
								; je�eli zakres nie zostanie przekroczony (czyli mo�emy zape�ni� rejestr) to nast�puje
								; skok do etykiety loopWithMMX, kt�ra wykonuje obliczenia stosuj�c rozkazy wektorowe
								; je�eli za� zakres zostanie przekroczony to nast�puje skok do etykiety loopWithoutMMX, w kt�rej
								; zostanie wykonane negowanie pozosta�ych warto�ci	
				
	add rdx, 20h				; tymczasowe dodanie 32 (rozmiar rejestru ymm) do adresu tablicy
	cmp r8, rdx					; sprawdzenie przekroczenia zakresu tablicy
	js changeLoop				; je�li adres przekroczy zakres edycji tablicy to przeskakujemy do etykiety changeLoop
	sub rdx, 20h				; je�li adres nie przekroczy zakresu edycji tablicy to odejmujemy 32
	jmp loopWithMMX				; skok do p�tli programu, kt�ra wykonuje operacje z wykorzystaniem rozkaz�w wektorowych
		
changeLoop:						; etykieta changeLoop, kt�rej jedynym zadaniem jest cofni�cie tymczasowego dodania 32 do adresu tablicy
								; a nast�pnie przeskoczenie do etykiety loopWithoutMMX
								
	sub rdx, 20h				; odj�cie 32 (rozmiar rejestru ymm) od adresu tablicy
	jmp loopWithoutMMX			; skok do p�tli programu, kt�ra nie wykonuje operacje bez wykorzystania rozkaz�w wektorowych

loopWithoutMMX:					; etykieta loopWithoutMMX, w kt�rej wykonywane s� te same operacje co w procedurze GenerateNegativeAsm
	mov al, [rcx+rdx]			; wczytanie kom�rki do 8 bitowego rejestru AL. Na pocz�tku jest to adres
								; kom�rki, od kt�rej ma zacza� by� edytowana tablica (czyli index pocz�tkowy + offset)
	xor al, neg_const			; negacja warto�ci, kt�ra si� znajdowa�a w rejestrze AL
	                            ; zastosowanie rozkazu xor zosta�o wyja�nione wy�ej
	mov [rcx+rdx], al           ; zapisanie zanegowanej warto�ci do jej miejsca w tablicy
	inc rdx						; inkrementacja indeksu tablicy
	cmp rdx, r8					; sprawdzenie czy zakres operacji na tablicy nie zosta� przekroczony
	je procEnd                  ; je�li zosta�o wykryte przekroczenie zakresu, to ko�czymy dzia�anie procedury
	                            ; skokiem do etykiety procEnd
	jmp loopWithoutMMX			; w innym przypadku (je�eli nie zosta� przekroczony zakres) to przeskakujemy
                                ; do pocz�tku p�tli mainLoop i wykonujemy dzia�anie dla kolejnej warto�ci
procEnd:                        
	ret	                        ; wyj�cie z procedury GenerateNegativeAsmAVX
	
GenerateNegativeAsmAVX endp

END